package com.fsarmiento.sample.devsupport.book

import com.fsarmiento.sample.devsupport.book.stub.LegacyAppStub

class BookServiceDevSupportApp {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val legacyAppStub = LegacyAppStub(8888)
            legacyAppStub.start()
            legacyAppStub.stubBook("test1234")
            legacyAppStub.stubBook("500", status = 500, priority = 99, responseFile = "")
            legacyAppStub.stubBook(".+", status = 404, priority = 100, responseFile = "/stub/book-not-found.json")

            val mainThread = Thread.currentThread()
            Runtime.getRuntime().addShutdownHook(object : Thread() {
                override fun run() {
                    legacyAppStub.stop()
                    mainThread.join()
                }
            })
        }
    }
}
