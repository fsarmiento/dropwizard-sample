package com.fsarmiento.sample.devsupport.book.stub

import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement

class LegacyAppStubRule(port: Int) : TestRule {
    val legacyAppStub = LegacyAppStub(port)

    override fun apply(base: Statement, description: Description): Statement {
        return object : Statement() {
            override fun evaluate() {
                try {
                    legacyAppStub.start()
                    base.evaluate()
                } finally {
                    legacyAppStub.stop()
                }
            }
        }
    }
}