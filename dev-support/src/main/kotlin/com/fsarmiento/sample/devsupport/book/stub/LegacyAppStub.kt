package com.fsarmiento.sample.devsupport.book.stub

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.client.WireMock.*
import com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig
import org.apache.http.HttpStatus.SC_OK
import org.slf4j.LoggerFactory

class LegacyAppStub(port: Int) {

    companion object {
        private val log = LoggerFactory.getLogger(LegacyAppStub::class.java)
        private const val PATH = "/api/v1/legacy/books"
    }

    private val wireMockServer = WireMockServer(wireMockConfig().port(port))
    private var wireMock = WireMock()

    fun start(): LegacyAppStub {
        wireMockServer.start()
        wireMock = WireMock(wireMockServer.port())
        log.info("Initialised legacy app stub on port ${wireMockServer.port()}")
        return this
    }

    fun stop(): LegacyAppStub {
        log.info("Shutting down legacy app stub running on port ${wireMockServer.port()}")
        wireMockServer.stop()
        return this
    }

    fun stubBook(id: String = "test1234",
                 status: Int = SC_OK,
                 priority: Int = 1,
                 responseFile: String = "/stub/$id.json",
                 fixedDelayInMs: Long = 0) {
        val request = get(urlPathMatching("$PATH/$id"))
        val response = aResponse().withStatus(status).withFixedDelay(fixedDelayInMs.toInt())
        if (responseFile.isNotBlank()) {
            val responseBody = javaClass.getResourceAsStream(responseFile)
                    .readBytes()

            response.withHeader("Content-Type", "application/json")
                    .withBody(responseBody)
        }

        wireMock.register(request.atPriority(priority)
                .willReturn(response))
    }

    fun verifyBookRequest(id: String, count: Int = 1) {
        val request = getRequestedFor(urlPathEqualTo("$PATH/$id"))
        wireMock.verifyThat(count, request)
    }

}

