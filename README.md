# Book Service

Sample app that uses dropwizard framework 

### Tools
* Oracle Java JDK 8 (1.8.0_91+)
* Kotlin 1.2+
* Gradle 4.5+
* Docker 17.12+

### Modules
* **app** - the book service application
* **dev-support** - used for local development that initialises Legacy App stub

### Building the app

From the root directory, run the command below:

```
./gradlew clean shadowJar
```

### Running the app locally

From the root directory, run the command below:

```
./gradlew run
```

This will start the application on port 9190, admin app on port 9191 and the Legacy App stub on port 8888.

To access the book app, click the endpoints below:

- [http://localhost:9190/api/v1/books/test1234](http://localhost:9190/api/v1/books/test1234) - retrieves book with id test1234
- [http://localhost:9190/api/v1/books/anything](http://localhost:9190/api/v1/books/anything) - simulates book not found
- [http://localhost:9190/api/v1/books/500](http://localhost:9190/api/v1/books/500) - simulates internal server error

Alternative, you can also use swagger:
- [http://localhost:9190/api/swagger](http://localhost:9190/api/swagger)

Use the admin endpoints to check for the application's state:
- [http://localhost:9191/api/book-service-ops/healthcheck](http://localhost:9191/api/book-service-ops/healthcheck) - health check
- [http://localhost:9191/api/book-service-ops/prometheus-metrics](http://localhost:9191/api/book-service-ops/prometheus-metrics) - health check

### Performance Testing

To run Jmeter tests:

```
./gradlew jmRun
```

By default, this will run Jmeter in local mode, pointing to a local instance of the app and report test metrics to local InfluxDB.

To point to a different instance of the app and influxDB, use the jmUserProperties to override the values:

```
./gradlew -DjmUserProperties="serviceHostname=<app under test hostname>,influxDBHostname=<influxdb hostname>" jmRun
```

To edit Jmeter tests:

```
./gradlew jmGui
```

To run Jmeter tests in remote hosts:

1. Set the remote_hosts in jmeter.properties
2. Uncomment *remote=true* in jmeter task property in perf-test/build.gradle

### Monitoring

Prometheus and Grafana are used to monitor the application, whilst InfluxDB and Grafana are used to monitor the performance tests.
To start local monitoring, run the command below from the root directory:

```
./monitor start
```

This will run Prometheus, InfluxDB and Grafana docker containers, set up the datasource and import the dashboard.

To stop monitoring, run the command below:

```
./monitor stop
```

#### To access Grafana:

Login with username: **admin** and password: **admin**

- [http://localhost:3002/dashboards](http://localhost:3002/dashboards)

Book Service dashboard is available to view metrics such as throughput, errors and circuit breaker

#### To access Prometheus:

- [http://localhost:9092](http://localhost:9092)


### Building docker image

To create a docker image, ensure that you have already built the app, then run the command below:

```
./gradlew docker
```

### Running docker image

```
docker run -it --name book-service -p 9190:9190 -p 9191:9191 fsarmiento/book-service:$version
```