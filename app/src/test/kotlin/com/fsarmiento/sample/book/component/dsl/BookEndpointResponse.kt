package com.fsarmiento.sample.book.component.dsl

import org.assertj.core.api.Assertions.assertThat
import javax.ws.rs.core.MediaType.APPLICATION_JSON
import javax.ws.rs.core.Response
import javax.ws.rs.core.Response.Status.*

/**
 * @author Florencio Sarmiento
 */
class BookEndpointResponse(val response: Response) {

    companion object {
        const val CACHE_HEADER_VALUE = "public, max-age=0"
    }

    fun then(): BookEndpointResponse {
        return this
    }

    fun expectOkStatus(): BookEndpointResponse {
        assertThat(response.status).isEqualTo(OK.statusCode)
        return this
    }

    fun expectInternalServerError(): BookEndpointResponse {
        assertThat(response.status).isEqualTo(INTERNAL_SERVER_ERROR.statusCode)
        return expectErrorMessage("${INTERNAL_SERVER_ERROR.statusCode}",
                "The system is currently unable to process your request")
    }

    fun expectNotFoundError(): BookEndpointResponse {
        assertThat(response.status).isEqualTo(NOT_FOUND.statusCode)
        return expectErrorMessage("not found", "Book was not found")
    }

    fun expectCorrectHeaders(): BookEndpointResponse {
        assertThat(response.getHeaderString("Content-Type")).isEqualTo(APPLICATION_JSON)
        assertThat(response.getHeaderString("Cache-Control")).isEqualTo(CACHE_HEADER_VALUE)
        return this
    }

    fun expectBookWithId(bookId: String): BookEndpointResponse {
        val responseBody = response.readEntity(Map::class.java)
        assertThat(responseBody["id"]).isEqualTo(bookId)
        assertThat(responseBody["tags"]).isNotNull
        assertThat(responseBody["tags"] as List<*>).containsExactly("tag1", "tag2")
        return this
    }

    private fun expectErrorMessage(code: String, errorMessage: String): BookEndpointResponse {
        val responseBody = response.readEntity(Map::class.java)
        assertThat(responseBody["code"]).isEqualTo(code)
        assertThat(responseBody["message"]).isEqualTo(errorMessage)
        return this
    }
}