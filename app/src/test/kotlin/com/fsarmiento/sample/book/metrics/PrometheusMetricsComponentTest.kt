package com.fsarmiento.sample.book.metrics

import com.fsarmiento.sample.book.BookServiceApp
import com.fsarmiento.sample.book.BookServiceConfig
import com.fsarmiento.sample.devsupport.book.stub.LegacyAppStubRule
import com.yammer.tenacity.testing.TenacityTestRule
import io.dropwizard.testing.junit.DropwizardAppRule
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.ClassRule
import org.junit.Rule
import org.junit.Test
import javax.ws.rs.client.Client
import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.Response.Status.OK

class PrometheusMetricsComponentTest {

    companion object {
        @ClassRule
        @JvmField
        val RULE = DropwizardAppRule<BookServiceConfig>(BookServiceApp::class.java, "app-config-component-test.yml")
    }

    @Rule
    @JvmField
    val tenacityRule = TenacityTestRule()

    @Rule
    @JvmField
    val legacyAppStubRule = LegacyAppStubRule(9299)

    private lateinit var client: Client
    private lateinit var target: WebTarget

    @Before
    fun setUp() {
        client = RULE.client()

        target = client
                .target("http://localhost:${RULE.adminPort}")
                .path("/api/book-service-ops/prometheus-metrics")
    }

    @Test
    fun `should increment metric count when retrieving the book by id`() {
        val bookId = "test1234"
        legacyAppStubRule.legacyAppStub.stubBook(bookId)

        val expectedCount = 3

        for (count in 1..expectedCount) {
            val response = client
                    .target("http://localhost:${RULE.localPort}")
                    .path("api/v1/books/$bookId")
                    .request().get()
            assertThat(response.status).isEqualTo(OK.statusCode)
        }

        assertMetricCount("com_fsarmiento_sample_book_resource_BookResource_getBookById_count", expectedCount)
    }

    private fun assertMetricCount(metricName: String, expectedCount: Int) {
        val response = target.request().get()

        assertThat(response.status).isEqualTo(OK.statusCode)
        assertThat(response.readEntity(String::class.java)).contains("$metricName ${expectedCount.toDouble()}")
    }
}