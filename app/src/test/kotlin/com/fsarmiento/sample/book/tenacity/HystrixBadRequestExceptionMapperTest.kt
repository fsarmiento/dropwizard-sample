package com.fsarmiento.sample.book.tenacity

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.JsonNodeFactory
import com.netflix.hystrix.exception.HystrixBadRequestException
import com.nhaarman.mockito_kotlin.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import javax.ws.rs.ClientErrorException
import javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE
import javax.ws.rs.core.Response
import javax.ws.rs.core.Response.Status.BAD_REQUEST

class HystrixBadRequestExceptionMapperTest {

    private val jsonNodeFactory = JsonNodeFactory.instance

    private lateinit var mockResponse: Response

    private lateinit var mockCatchAllExceptionMapper: CatchAllExceptionMapper

    private lateinit var classUnderTest: HystrixBadRequestExceptionMapper

    @Before
    fun setUp() {
        mockResponse = mock()
        mockCatchAllExceptionMapper = mock()

        classUnderTest = HystrixBadRequestExceptionMapper(mockCatchAllExceptionMapper)
    }

    @Test
    fun `should respond with the same error status and default message details when error info not available from client error exception`() {
        whenever(mockResponse.readEntity(JsonNode::class.java)).thenReturn(null)
        whenever(mockResponse.status).thenReturn(BAD_REQUEST.statusCode)
        whenever(mockResponse.statusInfo).thenReturn(BAD_REQUEST)

        val result = classUnderTest.toResponse(HystrixBadRequestException("Some error", ClientErrorException(mockResponse)))

        assertThat(result.status).isEqualTo(BAD_REQUEST.statusCode)
        assertThat(result.mediaType).isEqualTo(APPLICATION_JSON_TYPE)
        assertThat(result.entity).isEqualTo(Error(BAD_REQUEST.statusCode, "${BAD_REQUEST.statusCode}",
                "Unknown validation error has occurred"))

        verifyZeroInteractions(mockCatchAllExceptionMapper)
    }

    @Test
    fun `should respond with internal server error for any non-client error exception`() {
        classUnderTest.toResponse(HystrixBadRequestException("Some error"))

        verify(mockCatchAllExceptionMapper).toResponse(any())
    }
}