package com.fsarmiento.sample.book.documentation

import com.fsarmiento.sample.book.BookServiceApp
import com.fsarmiento.sample.book.BookServiceConfig
import com.fsarmiento.sample.book.resource.BookResource.Companion.GET_BOOK_BY_ID_OPERATION_ID
import com.fsarmiento.sample.devsupport.book.stub.LegacyAppStubRule
import com.yammer.tenacity.testing.TenacityTestRule
import io.dropwizard.testing.junit.DropwizardAppRule
import io.github.restdocsext.jersey.JerseyRestDocumentation.document
import io.github.restdocsext.jersey.JerseyRestDocumentation.documentationConfiguration
import io.github.swagger2markup.Swagger2MarkupConverter
import org.assertj.core.api.Assertions.assertThat
import org.junit.ClassRule
import org.junit.Rule
import org.junit.Test
import org.springframework.restdocs.JUnitRestDocumentation
import org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest
import org.springframework.restdocs.operation.preprocess.Preprocessors.removeHeaders
import java.net.URI
import java.nio.charset.Charset
import java.nio.file.Files
import java.nio.file.Paths
import javax.ws.rs.client.ClientBuilder
import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.MediaType.APPLICATION_JSON
import javax.ws.rs.core.Response.Status.OK

class DocGenerationTest {
    companion object {
        @ClassRule
        @JvmField
        val RULE = DropwizardAppRule<BookServiceConfig>(BookServiceApp::class.java, "app-config-component-test.yml")

        private const val SNIPPETS_DOC_DIR = "target/asciidoc/snippets"
        private const val GENERATED_DOC_DIR = "target/asciidoc/generated"
    }

    @Rule
    @JvmField
    val tenacityRule = TenacityTestRule()

    @Rule
    @JvmField
    val documentation: JUnitRestDocumentation = JUnitRestDocumentation(SNIPPETS_DOC_DIR)

    @Rule
    @JvmField
    val legacyAppStubRule = LegacyAppStubRule(9299)

    @Test
    fun `should generated docs from swagger`() {
        // must be done in this order to ensure docs generated from swagger contain the sample requests and response
        val getBookByIdResponseJson = generateAndAssertSampleRequestDocsForGetBookByIdEndpoint()
        generateAndAssertDocsAreGeneratedFromSwagger(getBookByIdResponseJson)
    }

    private fun generateAndAssertSampleRequestDocsForGetBookByIdEndpoint(): String {
        val bookId = "test1234"
        legacyAppStubRule.legacyAppStub.stubBook(bookId)

        val target = ClientBuilder.newClient()
                .target("http://localhost:${RULE.localPort}")
                .path("/api/v1/books/$bookId")

        return generateAndAssertSampleRequestDocs(GET_BOOK_BY_ID_OPERATION_ID, target)
    }

    private fun generateAndAssertSampleRequestDocs(operationId: String, webTarget: WebTarget): String {
        val response = webTarget
                .register(document(operationId, preprocessRequest(removeHeaders("User-Agent"))))
                .register(documentationConfiguration(documentation))
                .request(APPLICATION_JSON).get()

        assertThat(response.status).isEqualTo(OK.statusCode)

        val responseBody = response.readEntity(String::class.java)
        assertThat(responseBody).isNotBlank()
        assertDocsAreGenerated(operationId)

        return responseBody
    }

    private fun generateAndAssertDocsAreGeneratedFromSwagger(vararg expectedSampleJsonRequests: String) {
        System.setProperty("swagger2markup.extensions.springRestDocs.snippetBaseUri", SNIPPETS_DOC_DIR)

        Files.createDirectories(Paths.get(GENERATED_DOC_DIR))
        Swagger2MarkupConverter
                .from(URI("http://localhost:${RULE.localPort}/api/swagger.json"))
                .build()
                .toPath(Paths.get(GENERATED_DOC_DIR))

        assertThat(Files.exists(Paths.get(GENERATED_DOC_DIR, "definitions.adoc"))).isTrue()
        assertThat(Files.exists(Paths.get(GENERATED_DOC_DIR, "overview.adoc"))).isTrue()
        assertThat(Files.exists(Paths.get(GENERATED_DOC_DIR, "paths.adoc"))).isTrue()
        assertThat(Files.exists(Paths.get(GENERATED_DOC_DIR, "security.adoc"))).isTrue()

        val generatedPathsDoc = String(Files.readAllBytes(Paths.get(GENERATED_DOC_DIR, "paths.adoc")), Charset.forName("UTF-8"))
        expectedSampleJsonRequests.iterator().forEach { assertThat(generatedPathsDoc).contains(it) }
    }

    private fun assertDocsAreGenerated(subDir: String) {
        val fullPath = "$SNIPPETS_DOC_DIR/$subDir"
        assertThat(Files.exists(Paths.get(fullPath, "curl-request.adoc"))).isTrue()
        assertThat(Files.exists(Paths.get(fullPath, "http-request.adoc"))).isTrue()
        assertThat(Files.exists(Paths.get(fullPath, "http-response.adoc"))).isTrue()
    }
}