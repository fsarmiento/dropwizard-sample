package com.fsarmiento.sample.book.metrics

import com.fsarmiento.sample.book.BookServiceApp
import com.fsarmiento.sample.book.BookServiceConfig
import com.fsarmiento.sample.devsupport.book.stub.LegacyAppStubRule
import com.yammer.tenacity.testing.TenacityTestRule
import io.dropwizard.testing.junit.DropwizardAppRule
import org.assertj.core.api.Assertions.assertThat
import org.awaitility.Awaitility.await
import org.junit.Before
import org.junit.ClassRule
import org.junit.Rule
import org.junit.Test
import java.util.concurrent.TimeUnit.SECONDS
import javax.ws.rs.client.Client
import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.Response.Status.OK

class CacheMetricsComponentTest {

    companion object {
        @ClassRule
        @JvmField
        val RULE = DropwizardAppRule<BookServiceConfig>(BookServiceApp::class.java, "app-config-component-cache-enabled-test.yml")
    }

    @Rule
    @JvmField
    val tenacityRule = TenacityTestRule()

    @Rule
    @JvmField
    val legacyAppStubRule = LegacyAppStubRule(9299)

    private lateinit var client: Client
    private lateinit var target: WebTarget

    @Before
    fun setUp() {
        client = RULE.client()

        target = client
                .target("http://localhost:${RULE.adminPort}")
                .path("/api/book-service-ops/prometheus-metrics")
    }

    @Test
    fun `should increment cache hits, misses, puts and eviction metrics`() {
        val bookId = "test1234"
        legacyAppStubRule.legacyAppStub.stubBook(bookId)
        val expectedCount = 3

        val getBookByIdRequest = client
                .target("http://localhost:${RULE.localPort}")
                .path("api/v1/books/$bookId")
                .request()

        for (count in 1..expectedCount) {
            assertThat(getBookByIdRequest.get().status).isEqualTo(OK.statusCode)
        }

        await().atMost(5, SECONDS).untilAsserted {
            val response = target.request().get().readEntity(String::class.java)
            assertThat(response).contains("jcache_statistics_book_cache_cache_misses 1.0")
            assertThat(response).contains("jcache_statistics_book_cache_cache_puts 1.0")
            assertThat(response).contains("jcache_statistics_book_cache_cache_hits ${expectedCount.toDouble() - 1}")
        }

        await().atMost(5, SECONDS).untilAsserted {
            getBookByIdRequest.get()

            val response = target.request().get().readEntity(String::class.java)
            assertThat(response).contains("jcache_statistics_book_cache_cache_evictions 1.0")
        }
    }
}