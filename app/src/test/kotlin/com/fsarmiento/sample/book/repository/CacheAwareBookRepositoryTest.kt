package com.fsarmiento.sample.book.repository

import com.fsarmiento.sample.book.client.BookClient
import com.fsarmiento.sample.book.domain.Book
import com.github.benmanes.caffeine.jcache.CacheManagerImpl
import com.netflix.hystrix.exception.HystrixBadRequestException
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.whenever
import com.typesafe.config.ConfigFactory
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.verify
import java.util.concurrent.TimeUnit.MILLISECONDS
import javax.cache.CacheManager
import javax.cache.Caching
import javax.cache.spi.CachingProvider

class CacheAwareBookRepositoryTest {

    companion object {
        const val TEST_BOOK_ID = "test1234"
    }

    private val testBook = Book(TEST_BOOK_ID, "Some Book")

    private lateinit var mockBookClient: BookClient

    private lateinit var cachingProvider: CachingProvider

    private lateinit var cacheManager: CacheManager

    private lateinit var classUnderTest: CacheAwareBookRepository

    @Before
    fun setUp() {
        mockBookClient = mock()

        cachingProvider = Caching.getCachingProvider()
        cacheManager = CacheManagerImpl(cachingProvider, cachingProvider.defaultURI,
                cachingProvider.defaultClassLoader, cachingProvider.defaultProperties,
                ConfigFactory.load("book-cache-time-eviction-test.conf"))

        classUnderTest = CacheAwareBookRepository(mockBookClient, cacheManager)
    }

    @After
    fun tearDown() {
        cacheManager.cacheNames.forEach { cacheManager.destroyCache(it) }
    }

    @Test
    fun `should return book by id from legacy app and populate cache when it does not exist in cache yet`() {
        whenever(mockBookClient.getBookById(TEST_BOOK_ID)).thenReturn(testBook)

        // legacy app should be called here
        var result = classUnderTest.getBookById(TEST_BOOK_ID)
        assertThat(result).isEqualTo(testBook)

        // book now should be available from cache
        result = classUnderTest.getBookById(TEST_BOOK_ID)
        assertThat(result).isEqualTo(testBook)

        verify(mockBookClient, times(1)).getBookById(TEST_BOOK_ID)
    }

    @Test
    fun `should return book by id from legacy app and populate cache when cache expires`() {
        val classUnderTest = CacheAwareBookRepository(mockBookClient, cacheManager)
        whenever(mockBookClient.getBookById(TEST_BOOK_ID)).thenReturn(testBook)

        // legacy app should be called here
        var result = classUnderTest.getBookById(TEST_BOOK_ID)
        assertThat(result).isEqualTo(testBook)

        // book now should be available from cache
        result = classUnderTest.getBookById(TEST_BOOK_ID)
        assertThat(result).isEqualTo(testBook)

        // waiting for cache to expire
        MILLISECONDS.sleep(1100)

        // legacy app should be called here again
        result = classUnderTest.getBookById(TEST_BOOK_ID)
        assertThat(result).isEqualTo(testBook)

        // book should be available from cache again
        result = classUnderTest.getBookById(TEST_BOOK_ID)
        assertThat(result).isEqualTo(testBook)

        verify(mockBookClient, times(2)).getBookById(TEST_BOOK_ID)
    }

    @Test
    fun `should throw the same exception when book is not in cache and legacy app client throws an exception`() {
        whenever(mockBookClient.getBookById(TEST_BOOK_ID)).thenThrow(HystrixBadRequestException("Some issues with Legacy App"))

        assertThatThrownBy { classUnderTest.getBookById(TEST_BOOK_ID) }
                .isInstanceOf(HystrixBadRequestException::class.java)
                .hasMessage("Some issues with Legacy App")
    }
}