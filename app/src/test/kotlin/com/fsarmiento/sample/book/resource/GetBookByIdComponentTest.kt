package com.fsarmiento.sample.book.resource

import com.fsarmiento.sample.book.BookServiceApp
import com.fsarmiento.sample.book.BookServiceConfig
import com.fsarmiento.sample.book.component.dsl.BookEndpointRequest
import com.fsarmiento.sample.devsupport.book.stub.LegacyAppStubRule
import com.yammer.tenacity.testing.TenacityTestRule
import io.dropwizard.testing.junit.DropwizardAppRule
import org.junit.Before
import org.junit.ClassRule
import org.junit.Rule
import org.junit.Test
import javax.ws.rs.client.Client
import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR
import javax.ws.rs.core.Response.Status.NOT_FOUND

class GetBookByIdComponentTest {
    companion object {
        @ClassRule
        @JvmField
        val RULE = DropwizardAppRule<BookServiceConfig>(BookServiceApp::class.java, "app-config-component-test.yml")

        const val TEST_BOOK_ID = "test1234"
    }

    @Rule
    @JvmField
    val tenacityRule = TenacityTestRule()

    @Rule
    @JvmField
    val legacyAppStubRule = LegacyAppStubRule(9299)

    private val legacyAppStub = legacyAppStubRule.legacyAppStub

    private lateinit var client: Client
    private lateinit var target: WebTarget

    @Before
    fun setUp() {
        client = RULE.client()

        legacyAppStub.stubBook(TEST_BOOK_ID)

        target = client
                .target("http://localhost:${RULE.localPort}")
    }

    @Test
    fun `should return a 200 OK JSON response with cache headers when retrieving the book by id`() {
        givenABookEndpoint()
            .withId(TEST_BOOK_ID)
        .`when`()
            .get()
        .then()
            .expectOkStatus()
            .expectCorrectHeaders()
            .expectBookWithId(TEST_BOOK_ID)

        legacyAppStub.verifyBookRequest(TEST_BOOK_ID)
    }

    @Test
    fun `should return internal server error when getting book by id and an internal error occurs in the legacy app`() {
        legacyAppStub.stubBook(TEST_BOOK_ID, status = INTERNAL_SERVER_ERROR.statusCode)

        givenABookEndpoint()
            .withId(TEST_BOOK_ID)
        .`when`()
            .get()
        .then()
            .expectInternalServerError()
    }

    @Test
    fun `should return not found status for an unknown book id`() {
        legacyAppStub.stubBook(TEST_BOOK_ID, status = NOT_FOUND.statusCode, responseFile = "/stub/book-not-found.json")

        givenABookEndpoint()
            .withId(TEST_BOOK_ID)
        .`when`()
            .get()
        .then()
            .expectNotFoundError()
    }

    private fun givenABookEndpoint() : BookEndpointRequest {
        return BookEndpointRequest(target)
    }
}