package com.fsarmiento.sample.book.component.dsl

import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.MediaType

/**
 * @author Florencio Sarmiento
 */
class BookEndpointRequest(val target: WebTarget) {

    var id: String = ""

    fun withId(id: String): BookEndpointRequest {
        this.id = id
        return this
    }

    fun `when`(): BookEndpointRequest {
        return this
    }

    fun get(): BookEndpointResponse {
        val response = target.path("api/v1/books/$id")
                .request(MediaType.APPLICATION_JSON)
                .get()

        return BookEndpointResponse(response)
    }
}