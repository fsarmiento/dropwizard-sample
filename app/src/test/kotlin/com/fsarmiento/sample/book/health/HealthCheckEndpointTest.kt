package com.fsarmiento.sample.book.health

import com.fasterxml.jackson.databind.JsonNode
import com.fsarmiento.sample.book.BookServiceApp
import com.fsarmiento.sample.book.BookServiceConfig
import com.fsarmiento.sample.devsupport.book.stub.LegacyAppStubRule
import com.yammer.tenacity.testing.TenacityTestRule
import io.dropwizard.testing.junit.DropwizardAppRule
import org.assertj.core.api.Assertions.assertThat
import org.awaitility.Awaitility.await
import org.junit.Before
import org.junit.ClassRule
import org.junit.Rule
import org.junit.Test
import java.util.concurrent.TimeUnit.SECONDS
import javax.ws.rs.client.Client
import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.MediaType.APPLICATION_JSON
import javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR
import javax.ws.rs.core.Response.Status.OK

class HealthCheckEndpointTest {

    companion object {
        @ClassRule
        @JvmField
        val RULE = DropwizardAppRule<BookServiceConfig>(BookServiceApp::class.java, "app-config-reliability-test.yml")
    }

    @Rule
    @JvmField
    val tenacityRule = TenacityTestRule()

    @Rule
    @JvmField
    val legacyAppStubRule = LegacyAppStubRule(9399)

    private val legacyAppStub = legacyAppStubRule.legacyAppStub

    private lateinit var client: Client
    private lateinit var target: WebTarget

    @Before
    fun setUp() {
        client = RULE.client()

        target = client
                .target("http://localhost:${RULE.adminPort}")
                .path("/api/book-service-ops/healthcheck")
    }

    @Test
    fun `should return OK response and healthy`() {
        val response = target.request().get()

        assertThat(response.status).isEqualTo(OK.statusCode)
        val responseBody = response.readEntity(JsonNode::class.java)
        assertThat(responseBody.get("deadlocks")?.get("healthy")?.asBoolean()).isEqualTo(true)
        assertThat(responseBody.get("tenacity-circuitbreakers")?.get("healthy")?.asBoolean()).isEqualTo(true)
    }

    @Test
    fun `should return internal server error response with unhealthy circuit breaker when is tripped`() {
        val bookId = "test1234"
        legacyAppStub.stubBook(bookId, status = INTERNAL_SERVER_ERROR.statusCode)
        val getBookByIdRequest = client
                .target("http://localhost:${RULE.localPort}")
                .path("/api/v1/books/$bookId")

        await().atMost(30, SECONDS).untilAsserted {
            getBookByIdRequest.request(APPLICATION_JSON).get()
            val response = target.request().get()

            assertThat(response.status).isEqualTo(INTERNAL_SERVER_ERROR.statusCode)
            val responseBody = response.readEntity(JsonNode::class.java)
            assertThat(responseBody.get("deadlocks")?.get("healthy")?.asBoolean()).isEqualTo(true)
            assertThat(responseBody.get("tenacity-circuitbreakers")?.get("healthy")?.asBoolean()).isEqualTo(false)
        }
    }
}