package com.fsarmiento.sample.book.documentation

import com.fsarmiento.sample.book.BookServiceApp
import com.fsarmiento.sample.book.BookServiceConfig
import com.jayway.jsonpath.JsonPath
import com.yammer.tenacity.testing.TenacityTestRule
import io.dropwizard.testing.junit.DropwizardAppRule
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.ClassRule
import org.junit.Rule
import org.junit.Test
import javax.ws.rs.client.Client
import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.MediaType.APPLICATION_JSON
import javax.ws.rs.core.Response.Status.OK

class SwaggerTest {
    companion object {
        @ClassRule
        @JvmField
        val RULE = DropwizardAppRule<BookServiceConfig>(BookServiceApp::class.java, "app-config-component-test.yml")
    }

    @Rule
    @JvmField
    val tenacityRule = TenacityTestRule()

    private lateinit var client: Client
    private lateinit var target: WebTarget

    @Before
    fun setUp() {
        client = RULE.client()

        target = client
                .target("http://localhost:${RULE.localPort}")
                .path("/api")
    }

    @Test
    fun `should return swagger json with the correct endpoints`() {
        val response = target.path("/swagger.json").request(APPLICATION_JSON).get()

        assertThat(response.status).isEqualTo(OK.statusCode)

        val responseBody = response.readEntity(String::class.java)
        assertThat(getJsonPath(responseBody, "$.paths./v1/books/{bookId}")).isNotNull
    }

    @Test
    fun `should return swagger ui`() {
        val response = target.path("/swagger").request().get()

        assertThat(response.status).isEqualTo(OK.statusCode)
    }

    private fun getJsonPath(responseBody: String, path: String) {
        JsonPath.read<String>(responseBody, path)
    }
}