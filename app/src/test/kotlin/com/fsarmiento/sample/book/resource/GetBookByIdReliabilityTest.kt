package com.fsarmiento.sample.book.resource

import com.fsarmiento.sample.book.BookServiceApp
import com.fsarmiento.sample.book.BookServiceConfig
import com.fsarmiento.sample.devsupport.book.stub.LegacyAppStubRule
import com.fsarmiento.sample.book.tenacity.GetBookByIdKey
import com.netflix.hystrix.HystrixCircuitBreaker.Factory.getInstance
import com.netflix.hystrix.HystrixCommandKey.Factory.asKey
import com.yammer.tenacity.testing.TenacityTestRule
import io.dropwizard.testing.junit.DropwizardAppRule
import org.assertj.core.api.Assertions.assertThat
import org.awaitility.Awaitility.await
import org.junit.Before
import org.junit.ClassRule
import org.junit.Rule
import org.junit.Test
import java.util.concurrent.TimeUnit
import javax.ws.rs.client.Client
import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response.Status.*

class GetBookByIdReliabilityTest {
    companion object {
        @ClassRule
        @JvmField
        val RULE = DropwizardAppRule<BookServiceConfig>(BookServiceApp::class.java, "app-config-reliability-test.yml")

        const val TEST_BOOK_ID = "test1234"
    }

    @Rule
    @JvmField
    val tenacityRule = TenacityTestRule()

    @Rule
    @JvmField
    val legacyAppStubRule = LegacyAppStubRule(9399)

    private val legacyAppStub = legacyAppStubRule.legacyAppStub

    private lateinit var client: Client
    private lateinit var target: WebTarget

    @Before
    fun setUp() {
        client = RULE.client()

        target = client
                .target("http://localhost:${RULE.localPort}")
                .path("api/v1/books/$TEST_BOOK_ID")
    }

    @Test
    fun `should open circuit breaker when getting book and no of 5xx response received exceeds the set threshold`() {
        legacyAppStub.stubBook(TEST_BOOK_ID, status = INTERNAL_SERVER_ERROR.statusCode)
        assertThat(getInstance(asKey(GetBookByIdKey.name()))).isNull()

        await().atMost(5, TimeUnit.SECONDS).untilAsserted {
            val response = target.request(MediaType.APPLICATION_JSON).get()
            assertThat(response.status).isEqualTo(INTERNAL_SERVER_ERROR.statusCode)
            assertThat(getInstance(asKey(GetBookByIdKey.name())).isOpen).isTrue()
        }
    }

    @Test
    fun `should open circuit breaker when no of timed out requests exceeds the set threshold`() {
        legacyAppStub.stubBook(TEST_BOOK_ID, fixedDelayInMs = RULE.configuration.clientConfig.timeout.toMilliseconds() + 10)

        await().atMost(5, TimeUnit.SECONDS).untilAsserted {
            val response = target.request(MediaType.APPLICATION_JSON).get()
            assertThat(response.status).isEqualTo(INTERNAL_SERVER_ERROR.statusCode)
            assertThat(getInstance(asKey(GetBookByIdKey.name())).isOpen).isTrue()
        }
    }

    @Test
    fun `should not open circuit breaker when getting book and 4xx response is received`() {
        legacyAppStub.stubBook(TEST_BOOK_ID, status = BAD_REQUEST.statusCode)
        assertThat(getInstance(asKey(GetBookByIdKey.name()))).isNull()

        for (i in 1..10) {
            val response = target.request(MediaType.APPLICATION_JSON).get()
            assertThat(response.status).isEqualTo(BAD_REQUEST.statusCode)
            assertThat(getInstance(asKey(GetBookByIdKey.name())).isOpen).isFalse()
        }
    }

    @Test
    fun `should close circuit breaker after it opens and normal response resumed`() {
        legacyAppStub.stubBook(TEST_BOOK_ID, status = INTERNAL_SERVER_ERROR.statusCode)
        assertThat(getInstance(asKey(GetBookByIdKey.name()))).isNull()

        await().atMost(5, TimeUnit.SECONDS).untilAsserted {
            val response = target.request(MediaType.APPLICATION_JSON).get()
            assertThat(response.status).isEqualTo(INTERNAL_SERVER_ERROR.statusCode)
            assertThat(getInstance(asKey(GetBookByIdKey.name())).isOpen).isTrue()
        }

        legacyAppStub.stubBook(TEST_BOOK_ID)

        await().atMost(10, TimeUnit.SECONDS).untilAsserted {
            val response = target.request(MediaType.APPLICATION_JSON).get()
            assertThat(response.status).isEqualTo(OK.statusCode)
            assertThat(getInstance(asKey(GetBookByIdKey.name())).isOpen).isFalse()
        }
    }
}