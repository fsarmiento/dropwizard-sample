package com.fsarmiento.sample.book.repository

import com.fsarmiento.sample.book.domain.Book

interface BookRepository {
    fun getBookById(bookId: String): Book
}