package com.fsarmiento.sample.book.client

import com.fsarmiento.sample.book.client.command.GetBookByIdCommand
import com.fsarmiento.sample.book.domain.Book
import com.fsarmiento.sample.book.repository.BookRepository
import javax.ws.rs.client.WebTarget

class BookClient(target: WebTarget) : BookRepository {
    private val bookTarget: WebTarget = target.path("/api/v1/legacy/books")

    override fun getBookById(bookId: String): Book {
        val command = GetBookByIdCommand(bookTarget, bookId)
        return command.execute()
    }
}