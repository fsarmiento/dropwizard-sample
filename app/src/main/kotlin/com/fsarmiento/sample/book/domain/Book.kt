package com.fsarmiento.sample.book.domain

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonInclude.Include.NON_ABSENT
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

@JsonInclude(NON_ABSENT)
@JsonIgnoreProperties(ignoreUnknown = true)
data class Book(@get:JsonProperty("id") val id: String?,
                @get:JsonProperty("name") val name: String?,
                @get:JsonProperty("tags") var tags: List<String>? = emptyList()) : Serializable
