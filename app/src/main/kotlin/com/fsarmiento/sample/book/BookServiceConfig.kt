package com.fsarmiento.sample.book

import com.fasterxml.jackson.annotation.JsonProperty
import com.yammer.tenacity.core.config.TenacityConfiguration
import io.dropwizard.Configuration
import io.dropwizard.client.JerseyClientConfiguration
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration
import org.hibernate.validator.constraints.NotEmpty
import org.jetbrains.annotations.NotNull
import javax.validation.Valid
import javax.validation.constraints.Min

class BookServiceConfig : Configuration() {
    @Valid
    @NotNull
    lateinit var clientConfig: JerseyClientConfiguration

    @Valid
    @NotNull
    lateinit var cacheConfig: CacheConfig

    @NotEmpty
    lateinit var legacyAppUrl: String

    @Valid
    @NotNull
    lateinit var bookById: TenacityConfiguration

    @JsonProperty("swagger")
    lateinit var swaggerBundleConfiguration: SwaggerBundleConfiguration
}

class CacheConfig : Configuration() {

    @Valid
    @NotNull
    @Min(0)
    var maxAgeInSec: Int = 0

    @Valid
    @NotNull
    lateinit var cacheConfigFile: String
}