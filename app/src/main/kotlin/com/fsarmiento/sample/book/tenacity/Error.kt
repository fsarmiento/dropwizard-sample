package com.fsarmiento.sample.book.tenacity

data class Error(val status: Int, val code: String, val message: String)