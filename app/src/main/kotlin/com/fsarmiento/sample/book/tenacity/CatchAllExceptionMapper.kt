package com.fsarmiento.sample.book.tenacity

import org.slf4j.LoggerFactory
import javax.ws.rs.NotFoundException
import javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE
import javax.ws.rs.core.Response
import javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR
import javax.ws.rs.core.Response.Status.NOT_FOUND
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider

@Provider
class CatchAllExceptionMapper : ExceptionMapper<Exception> {

    companion object {
        private val log = LoggerFactory.getLogger(CatchAllExceptionMapper::class.java)
    }

    override fun toResponse(ex: Exception): Response {
        log.error("An error has occurred", ex)

        return when (ex) {
            is NotFoundException -> Response.status(NOT_FOUND)
                    .type(APPLICATION_JSON_TYPE)
                    .entity(Error(NOT_FOUND.statusCode, "${NOT_FOUND.statusCode}", "Not Found"))
                    .build()

            else -> Response.serverError()
                    .type(APPLICATION_JSON_TYPE)
                    .entity(Error(INTERNAL_SERVER_ERROR.statusCode, "${INTERNAL_SERVER_ERROR.statusCode}",
                            "The system is currently unable to process your request"))
                    .build()
        }
    }
}