package com.fsarmiento.sample.book

import com.codahale.metrics.jcache.JCacheGaugeSet
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fsarmiento.sample.book.client.BookClient
import com.fsarmiento.sample.book.repository.CacheAwareBookRepository
import com.fsarmiento.sample.book.resource.BookResource
import com.fsarmiento.sample.book.resource.CacheHeadersFilter
import com.fsarmiento.sample.book.tenacity.BookTenacityBundleConfigurationFactory
import com.fsarmiento.sample.book.tenacity.CatchAllExceptionMapper
import com.fsarmiento.sample.book.tenacity.HystrixBadRequestExceptionMapper
import com.github.benmanes.caffeine.jcache.CacheManagerImpl
import com.typesafe.config.ConfigFactory
import com.yammer.tenacity.core.bundle.TenacityBundleBuilder
import io.dropwizard.Application
import io.dropwizard.assets.AssetsBundle
import io.dropwizard.client.JerseyClientBuilder
import io.dropwizard.configuration.EnvironmentVariableSubstitutor
import io.dropwizard.configuration.ResourceConfigurationSourceProvider
import io.dropwizard.configuration.SubstitutingSourceProvider
import io.dropwizard.setup.Bootstrap
import io.dropwizard.setup.Environment
import io.federecio.dropwizard.swagger.SwaggerBundle
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration
import io.prometheus.client.Collector
import io.prometheus.client.CollectorRegistry
import io.prometheus.client.dropwizard.DropwizardExports
import io.prometheus.client.exporter.MetricsServlet
import javax.cache.Caching

class BookServiceApp : Application<BookServiceConfig>() {
    companion object {

        @JvmStatic
        fun main(vararg args: String) {
            BookServiceApp().run(*args)
        }
    }

    override fun initialize(bootstrap: Bootstrap<BookServiceConfig>) {
        bootstrap.apply {
            configurationSourceProvider = SubstitutingSourceProvider(ResourceConfigurationSourceProvider(), EnvironmentVariableSubstitutor(false))
            objectMapper.registerModule(KotlinModule())
            addBundle(object : SwaggerBundle<BookServiceConfig>() {
                override fun getSwaggerBundleConfiguration(configuration: BookServiceConfig): SwaggerBundleConfiguration {
                    return configuration.swaggerBundleConfiguration
                }
            })

            addBundle(AssetsBundle("/assets", "/", "index.html"))

            addBundle(TenacityBundleBuilder
                    .newBuilder<BookServiceConfig>()
                    .configurationFactory(BookTenacityBundleConfigurationFactory())
                    .withCircuitBreakerHealthCheck()
                    .usingAdminPort()
                    .build())
        }
    }

    override fun run(configuration: BookServiceConfig, environment: Environment) {
        val client = JerseyClientBuilder(environment).using(configuration.clientConfig).build("book-service")
        val webTarget = client.target(configuration.legacyAppUrl)
        val bookClient = BookClient(webTarget)

        val cachingProvider = Caching.getCachingProvider()
        val cacheManager = CacheManagerImpl(cachingProvider, cachingProvider.defaultURI,
                cachingProvider.defaultClassLoader, cachingProvider.defaultProperties,
                ConfigFactory.load(configuration.cacheConfig.cacheConfigFile))
        val cacheAwareBookRepository = CacheAwareBookRepository(bookClient, cacheManager)

        environment.jersey().apply {
            register(BookResource(cacheAwareBookRepository))
            register(CacheHeadersFilter(configuration.cacheConfig))

            val catchAllExceptionMapper = CatchAllExceptionMapper()
            register(catchAllExceptionMapper)
            register(HystrixBadRequestExceptionMapper(catchAllExceptionMapper))
        }

        registerPrometheusMetricsExporter(environment)
    }

    private fun registerPrometheusMetricsExporter(environment: Environment) {
        CollectorRegistry.defaultRegistry.clear()

        val metricRegistry = environment.metrics()
        metricRegistry.register("jcache.statistics", JCacheGaugeSet())
        DropwizardExports(metricRegistry).register<Collector>()

        environment.admin()
                .addServlet("prometheusMetrics", MetricsServlet(CollectorRegistry.defaultRegistry))
                .addMapping("/prometheus-metrics")
    }
}