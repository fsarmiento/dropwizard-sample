package com.fsarmiento.sample.book.repository

import com.fsarmiento.sample.book.domain.Book
import javax.cache.CacheManager

class CacheAwareBookRepository(private val delegate: BookRepository, cacheManager: CacheManager) : BookRepository {

    private val cache = cacheManager.getCache<String, Book>("book-cache", String::class.java, Book::class.java)

    override fun getBookById(bookId: String): Book {
        return cache.get(bookId) ?: putAndGet(bookId, delegate.getBookById(bookId))
    }

    private fun putAndGet(key: String, book: Book): Book {
        cache.put(key, book)
        return book
    }
}