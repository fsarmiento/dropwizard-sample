package com.fsarmiento.sample.book.tenacity

import com.fasterxml.jackson.databind.JsonNode
import com.netflix.hystrix.exception.HystrixBadRequestException
import org.slf4j.LoggerFactory
import javax.ws.rs.ClientErrorException
import javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE
import javax.ws.rs.core.Response
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider

@Provider
class HystrixBadRequestExceptionMapper(private val catchAllExceptionMapper: CatchAllExceptionMapper) : ExceptionMapper<HystrixBadRequestException> {

    companion object {
        private val log = LoggerFactory.getLogger(HystrixBadRequestExceptionMapper::class.java)
    }

    override fun toResponse(ex: HystrixBadRequestException): Response {
        log.debug("An error has occurred", ex)

        return when (ex.cause) {
            is ClientErrorException -> {
                val clientErrorException = ex.cause as ClientErrorException
                Response.status(clientErrorException.response.status)
                        .type(APPLICATION_JSON_TYPE)
                        .entity(getError(clientErrorException.response))
                        .build()
            }
            else -> catchAllExceptionMapper.toResponse(ex)
        }
    }

    private fun getError(response: Response): Error {
        val errorResponse = response.readEntity(JsonNode::class.java)?.get("response")?.get(0)
        val code = errorResponse?.get("status")?.asText() ?: response.status.toString()
        val message = errorResponse?.get("message")?.asText() ?: "Unknown validation error has occurred"
        return Error(response.status, code, message)
    }
}