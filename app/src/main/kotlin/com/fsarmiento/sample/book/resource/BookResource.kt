package com.fsarmiento.sample.book.resource

import com.codahale.metrics.annotation.Timed
import com.fsarmiento.sample.book.domain.Book
import com.fsarmiento.sample.book.repository.BookRepository
import io.swagger.annotations.Api
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.PathParam
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType.APPLICATION_JSON

@Api(value = "Books Endpoint")
@Path("/v1/books")
class BookResource(private val bookRepository: BookRepository) {

    companion object {
        const val GET_BOOK_BY_ID_OPERATION_ID = "getBookById"
    }

    @Timed
    @GET
    @Path("/{bookId}")
    @Produces(APPLICATION_JSON)
    fun getBookById(@PathParam("bookId") bookId: String): Book {
        Thread.sleep(5000)
        val book = bookRepository.getBookById(bookId)
        book.tags = listOf("tag1", "tag2")
        return book
    }
}