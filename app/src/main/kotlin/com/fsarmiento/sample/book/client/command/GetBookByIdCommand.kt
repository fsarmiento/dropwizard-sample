package com.fsarmiento.sample.book.client.command

import com.fsarmiento.sample.book.domain.Book
import com.fsarmiento.sample.book.tenacity.GetBookByIdKey
import com.netflix.hystrix.exception.HystrixBadRequestException
import com.yammer.tenacity.core.TenacityCommand
import org.slf4j.LoggerFactory
import javax.ws.rs.ClientErrorException
import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.MediaType.APPLICATION_JSON

class GetBookByIdCommand(private val target: WebTarget,
                         private val bookId: String) : TenacityCommand<Book>(GetBookByIdKey) {
    companion object {
        private val log = LoggerFactory.getLogger(GetBookByIdCommand::class.java)
    }

    override fun run(): Book {
        try {
            return target.path(bookId)
                    .request(APPLICATION_JSON)
                    .get(Book::class.java)

        } catch (ex: ClientErrorException) {
            log.debug("An error response received from Legacy App with status [${ex.response.status}] and response body [${ex.response.readEntity(String::class.java)}]")
            throw HystrixBadRequestException("An error occurred calling legacy app", ex)
        }
    }
}