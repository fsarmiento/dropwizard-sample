package com.fsarmiento.sample.book.tenacity

import com.yammer.tenacity.core.properties.TenacityPropertyKey

sealed class BookDependencyKeys : TenacityPropertyKey

object GetBookByIdKey : BookDependencyKeys() {
    override fun name(): String {
        return "GetBookCommand"
    }
}