package com.fsarmiento.sample.book.resource


import com.fsarmiento.sample.book.CacheConfig
import javax.ws.rs.container.ContainerRequestContext
import javax.ws.rs.container.ContainerResponseContext
import javax.ws.rs.container.ContainerResponseFilter
import javax.ws.rs.core.HttpHeaders.CACHE_CONTROL
import javax.ws.rs.core.Response.Status.Family.SUCCESSFUL
import javax.ws.rs.ext.Provider

@Provider
class CacheHeadersFilter(private val cacheHeaderConfig: CacheConfig) : ContainerResponseFilter {
    override fun filter(requestContext: ContainerRequestContext, responseContext: ContainerResponseContext) {
        if (SUCCESSFUL == responseContext.statusInfo?.family) {
            responseContext.headers.putSingle(CACHE_CONTROL, "public, max-age=${cacheHeaderConfig.maxAgeInSec}")
        }
    }
}