package com.fsarmiento.sample.book.tenacity

import com.fsarmiento.sample.book.BookServiceConfig
import com.google.common.collect.ImmutableMap
import com.yammer.tenacity.core.bundle.BaseTenacityBundleConfigurationFactory
import com.yammer.tenacity.core.config.TenacityConfiguration
import com.yammer.tenacity.core.properties.StringTenacityPropertyKeyFactory
import com.yammer.tenacity.core.properties.TenacityPropertyKey
import com.yammer.tenacity.core.properties.TenacityPropertyKeyFactory

class BookTenacityBundleConfigurationFactory : BaseTenacityBundleConfigurationFactory<BookServiceConfig>() {
    override fun getTenacityPropertyKeyFactory(applicationConfiguration: BookServiceConfig): TenacityPropertyKeyFactory {
        return StringTenacityPropertyKeyFactory()
    }

    override fun getTenacityConfigurations(applicationConfiguration: BookServiceConfig): MutableMap<TenacityPropertyKey, TenacityConfiguration> {
        val builder: ImmutableMap.Builder<TenacityPropertyKey, TenacityConfiguration> = ImmutableMap.builder()
        builder.put(GetBookByIdKey, applicationConfiguration.bookById)

        return builder.build()
    }
}